import { AmbientLight, OrthographicCamera, Scene, Vector3, WebGLRenderer, sRGBEncoding } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

function easeOutCirc(x) {
	return Math.sqrt(1 - Math.pow(x - 1, 4));
}

function loadGLTFModel(scene, glbPath, options) {
	const { receiveShadow, castShadow } = options;
	return new Promise((resolve, reject) => {
		const loader = new GLTFLoader();
		loader.load(
			glbPath,
			(gltf) => {
				const obj = gltf.scene;
				obj.name = "dinosaur";
				obj.position.y = 0;
				obj.position.x = 0;
				obj.receiveShadow = receiveShadow;
				obj.castShadow = castShadow;
				scene.add(obj);

				obj.traverse(function (child) {
					if ((child as any).isMesh) {
						child.castShadow = castShadow;
						child.receiveShadow = receiveShadow;
					}
				});

				resolve(obj);
			},
			undefined,
			function (error) {
				console.log(error);
				reject(error);
			}
		);
	});
}
export class ThreeJSApp {
	camera: OrthographicCamera;
	controls: OrbitControls;
	resize() {
		const scW = this.container.clientWidth;
		const scH = this.container.clientHeight;
	
		let frustumSize = 10.0;
		let aspect = scW / scH;
		this.camera.left = frustumSize * aspect / - 2.0;
		this.camera.right = frustumSize * aspect / 2.0;
		this.camera.top = frustumSize / 2.0;
		this.camera.bottom = frustumSize / - 2.0;
		this.renderer.setSize(scW, scH);
		this.camera.updateProjectionMatrix();
	}
	private renderer: WebGLRenderer;
	req: any;
	container: HTMLDivElement;
	scene: Scene;
	destroy() {
		cancelAnimationFrame(this.req);
		this.renderer.dispose();
		this.scene.clear();
		//console.error("DISPOSE");
		this.container.removeChild(this.renderer.domElement);
	}
	render(dt: number)
	{

		this.controls.update();
		
		this.renderer.render(this.scene, this.camera);
	}
	constructor(container: HTMLDivElement, [loading, setLoading]: [boolean, (boolean) => void]) {
		if (container && !this.renderer) {
			this.container = container;
			const scW = container.clientWidth;
			const scH = container.clientHeight;
			this.renderer = new WebGLRenderer({
				antialias: true,
				alpha: true
			});
			this.renderer.setPixelRatio(window.devicePixelRatio);
			this.renderer.setSize(scW, scH);
			this.renderer.outputEncoding = sRGBEncoding;
			container.appendChild(this.renderer.domElement);

			const scene = new Scene();
			this.scene = scene;
			const scale = 5.6;
			var aspect = scW / scH;
			const camera = new OrthographicCamera(
				-scale,
				scale,
				scale,
				-scale,
				-500,
				500
			);
			this.camera = camera;
			camera.position.y = 1.5;
			this.resize();
			const target = new Vector3(-0.5, 1.2, 0);

			const ambientLight = new AmbientLight(0xcccccc, 1);
			scene.add(ambientLight);
			const controls = new OrbitControls(camera, this.renderer.domElement);
			//controls.autoRotate = true;
			controls.target = target;
			this.controls = controls;

			loadGLTFModel(scene, "/Dinosaur.glb", {
				receiveShadow: false,
				castShadow: false
			}).then(() => {
				animate(0);
				setLoading(false);
			});

			this.req = null;
			let frame = 0;
			const animate = (dt?: number) => {
				this.req = requestAnimationFrame(animate);
				
				if (dt != undefined)
				{
					this.render(dt);
				}
				else {
					
				}

			};
		}
	}
}