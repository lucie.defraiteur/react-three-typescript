import { useState, useEffect, useRef } from "react";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { ThreeJSApp } from "./ThreeJSApp";
import "./App.css";

const ThreeAppRoot = () => {
  const refContainer: React.MutableRefObject<HTMLDivElement> = useRef();
  const [loading, setLoading] = useState(true);
  useEffect(() => {

      let threeApp = new ThreeJSApp(refContainer.current, [loading, setLoading])
      let ro = new ResizeObserver((entries) => {
        threeApp.resize();
      });
      ro.observe(refContainer.current);
      return () => {
        if (threeApp)
        {
          threeApp.destroy();
        }
      };
    }
  , []);
  return (
    <div
      style={{ height: "100%", width: "100%", position: "relative", justifyContent:"center", backgroundColor:"gray" }}
      ref={refContainer}
    >
      {loading && (
        <span style={{ position: "absolute", left: "50%", top: "50%" }}>
          Loading...
        </span>
      )}
    </div>
  );
};

export default function App() {
  return (
    <div style={{ 
      height: "100vh%",
      width: "100vw",

    background:"yellow"}}>
      <ThreeAppRoot />
    </div>
  );
}
